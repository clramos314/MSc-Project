from snakemake.io import expand, touch

#singularity: "docker://continuumio/miniconda3:4.6.14"

###### Config file and sample sheets #####
configfile: "config.yaml"

OUTDIR = config["outdir"]
LOGDIR = config["logdir"]

# sample(s) from the same individual
# SAMPLES = pd.read_csv(config["samples"], sep="\t").set_index("sample", drop=False)
SAMPLES = ['TCGA-38-4629-01A-02D-1265-08']

include: "rules/common.smk"

##### Target rules #####

rule all:
    input:
        expand(f'{OUTDIR}/pyclone-vi/input_{{sample}}.tsv', sample=SAMPLES),
        expand(f'{OUTDIR}/annotated/snv_{{sample}}.vcf',sample=SAMPLES),
        expand(f'{OUTDIR}/pyclone-vi/output_{{sample}}.tsv',sample=SAMPLES),
        expand(f'{OUTDIR}/pandrugs/{{sample}}_computations.csv',sample=SAMPLES)

rule pre_process_piv:
    input:
        SNVs = expand(f'{OUTDIR}/annotated/snv_{{sample}}.vcf', sample=SAMPLES),
        CNVs = expand(f'{OUTDIR}/annotated/cnv_{{sample}}.tsv', sample=SAMPLES)
    params:
        sample = expand(f'{{sample}}', sample=SAMPLES)
    output:
        expand(f'{OUTDIR}/pyclone-vi/input_{{sample}}.tsv', sample=SAMPLES)
    conda: "envs/commons.yaml"
    threads: config["resources"]["pyclone"]["threads"]
    resources:
        mem = config["resources"]["pyclone"]["mem"],
        walltime=config["resources"]["pyclone"]["walltime"]
    log:
        out = f"{LOGDIR}/pyclone-vi/pre-process.log",
        err = f"{LOGDIR}/pyclone-vi/pre-process.err"
    shell:"""
        python scripts/cross_snv_cnv.py {params.sample} {input.SNVs} {input.CNVs} {output} > {log} 2> {log}
    """

rule piv:
    input:
        expand(f'{OUTDIR}/pyclone-vi/input_{{sample}}.tsv', sample=SAMPLES)
    output:
        fit = expand(f'{OUTDIR}/pyclone-vi/output_{{sample}}.h5', sample=SAMPLES),
        result = expand(f'{OUTDIR}/pyclone-vi/output_{{sample}}.tsv', sample=SAMPLES)
    conda:"envs/pyclone-vi.yaml"
    threads: config["resources"]["pyclone"]["threads"]
    resources:
        mem = config["resources"]["pyclone"]["mem"],
        walltime = config["resources"]["pyclone"]["walltime"]
    shell:"""
        pyclone-vi fit -i {input} -o {output.fit} -c 40 -d beta-binomial -r 10 &&
        pyclone-vi write-results-file -i {output.fit} -o {output.result}
    """

rule req_variant_analysis:
    input:
        SNVs = expand(f'{OUTDIR}/annotated/snv_{{sample}}.vcf',sample=SAMPLES),
        PIVs = expand(f'{OUTDIR}/pyclone-vi/output_{{sample}}.tsv',sample=SAMPLES)
    output:
        out=touch(f'{OUTDIR}/pandrugs/{{sample}}_computations.csv')
    params:
        sample = expand(f'{{sample}}',sample=SAMPLES),
        output = expand(f'{OUTDIR}/pandrugs/{{sample}}_computations.csv',sample=SAMPLES)
    conda: "envs/pandrugs.yaml"
    threads: config["resources"]["pandrugs"]["threads"]
    resources:
        mem = config["resources"]["pandrugs"]["mem"],
        walltime = config["resources"]["pandrugs"]["walltime"]
    log:
        out = f"{LOGDIR}/pandrugs/va_out_{{sample}}.log",
        err = f"{LOGDIR}/pandrugs/va_err_{{sample}}.err"
    shell: """
        python scripts/clones2pandrugs.py {params.sample} {input.SNVs} {input.PIVs} {params.output} > {log.out} 2> {log.err}
    """
