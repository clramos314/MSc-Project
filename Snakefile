import pandas as pd
from snakemake.io import expand, touch

###### Config file and sample sheets #####
configfile: "config.yaml"

OUTDIR = config["outdir"]
LOGDIR = config["logdir"]
VEP_DATA = config["vepdatadir"]
DATASET_DIR = config["dataset_dir"]
DATASET = config["dataset"]
BASE_DIR = config["base_dir"]

PATIENT = '47'

# sample(s) from the same individual
CASES = pd.read_csv(DATASET, sep="\t").set_index("case", drop=False)
SAMPLES = [getattr(row, 'case') for row in CASES.itertuples()]

include: "rules/common.smk"

##### Target rules #####

rule all:
    input:
        f'{OUTDIR}/post_pyclone.done',
        f'{OUTDIR}/make_vcf.done',
        f'{OUTDIR}/make_pandrugs.done',
        f'{OUTDIR}/pre_citup.done'


rule pre_pyclone:
    input:
        samples_tsv = f'{DATASET}',
        all_mutations = f'{DATASET_DIR}/mutations/mutations.tsv'
    output:
        expand(f'{OUTDIR}/pyclone/{{sample}}.tsv', sample=SAMPLES)
    params:
        input_dir = f'{DATASET_DIR}',
        output_dir = f'{OUTDIR}/pyclone'
    conda:"envs/commons.yaml"
    shell:
        "python scripts/gen_input_pyclone_from_paradiff.py --input_dir {params.input_dir} --samples {input.samples_tsv} --mutations {input.all_mutations} --output_dir {params.output_dir}"


rule pyclone:
    input:
        expand(f'{OUTDIR}/pyclone/{{sample}}.tsv', sample=SAMPLES)
    output:
        file = expand(f'{OUTDIR}/pyclone/{{sample}}/tables/loci.tsv', sample=SAMPLES)
    params:
        samples_tsv = f'{DATASET}',
        in_dir = f'{BASE_DIR}',
        out_dir = f'{BASE_DIR}/{OUTDIR}/pyclone'
    conda:"envs/pyclone.yaml"
    threads: config["resources"]["pyclone"]["threads"]
    resources:
        mem = config["resources"]["pyclone"]["mem"],
        walltime = config["resources"]["pyclone"]["walltime"]
    shell:
        "python scripts/run_pyclone.py --samples {params.samples_tsv} --in_dir {params.in_dir} --in_files {input} --out_dir {params.out_dir}"


rule post_pyclone:
    input:
        loci = expand(f'{OUTDIR}/pyclone/{{sample}}/tables/loci.tsv', sample=SAMPLES),
        all_mutations = f'{DATASET_DIR}/mutations/mutations.tsv'
    output:
        flag = touch(f'{OUTDIR}/post_pyclone.done')
    params:
        out_dir = f'{VEP_DATA}',
        samples_tsv = f'{DATASET}'
    conda:"envs/commons.yaml"
    shell:
        "python scripts/gen_input_vep_from_pyclone_paradiff.py --loci {input.loci} --mutations {input.all_mutations} --out_dir {params.out_dir} --samples {params.samples_tsv}"


rule make_vcf:
    input:
        f'{OUTDIR}/post_pyclone.done'
    output:
        flag = touch(f'{OUTDIR}/make_vcf.done')
    params:
        clusters = expand(f'{BASE_DIR}/{OUTDIR}/pyclone/{{sample}}/tables/cluster.tsv', sample=SAMPLES),
        vep_dir = f'{VEP_DATA}'
    conda:
        "envs/pandrugs.yaml"
    shell:
        "python scripts/run_docker_vep.py --clusters {params.clusters} --vep_dir {params.vep_dir}"


rule req_variant_analysis:
    input:
        f'{OUTDIR}/make_vcf.done'
    output:
        flag = touch(f'{OUTDIR}/make_pandrugs.done')
    params:
        clusters = expand(f'{BASE_DIR}/{OUTDIR}/pyclone/{{sample}}/tables/cluster.tsv',sample=SAMPLES),
        vep_dir = f'{VEP_DATA}',
        out_dir = f'{BASE_DIR}/{OUTDIR}/pandrugs'
    conda:
        "envs/pandrugs.yaml"
    threads: config["resources"]["pandrugs"]["threads"]
    resources:
        mem = config["resources"]["pandrugs"]["mem"],
        walltime = config["resources"]["pandrugs"]["walltime"]
    shell:
        "python scripts/clones_to_pandrugs_from_pyclone_vep_paradiff.py --clusters {params.clusters} --vep_dir {params.vep_dir} --out_dir {params.out_dir}"


rule pre_citup_all_patient_samples:
    input:
        f'{OUTDIR}/post_pyclone.done',
        loci= f'{BASE_DIR}/{OUTDIR}/pyclone/patient_{PATIENT}/tables/loci.tsv'
    output:
        flag = touch(f'{OUTDIR}/pre_citup.done')
    params:
        out_dir = f'{BASE_DIR}/{OUTDIR}/citup'
    conda:
        "envs/commons.yaml"
    shell:
        "python scripts/gen_input_citup_from_all_samples_pyclone_paradiff.py --loci {input.loci} --out_dir {params.out_dir}"


rule run_citup_all_patient_samples:
    input:
        f'{OUTDIR}/post_pyclone.done'
    output:
        flag = touch(f'{OUTDIR}/pre_citup.done')
    params:
        out_dir = f'{BASE_DIR}/{OUTDIR}/citup'
    conda:
        "envs/citup.yaml"
    shell:
        "run_citup_iter.py out/citup/freq.txt out/citup/results.h5 --submit local"


rule pre_mapscape:
    input:
        f'{OUTDIR}/run_citup.done'
    output:
        flag = touch(f'{OUTDIR}/pre_mapscape.done')
    params:
        in_file = f'{BASE_DIR}/{OUTDIR}/mapscape/results.h5',
        loci= f'{BASE_DIR}/{OUTDIR}/pyclone/patient_{PATIENT}/tables/loci.tsv',
        out_dir = f'{BASE_DIR}/{OUTDIR}/mapscape/data'
    conda:
        "envs/mapscape.yaml"
    shell:
        "python scripts/gen_input_e_scape_from_pyclone_citup_paradiff.py --in_file {params.in_file} --loci {params.loci} --out_dir {params.out_dir}"
